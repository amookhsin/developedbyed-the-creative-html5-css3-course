# پایه HTML

در سند HTML هر محتوایی بین تگ‌های باز-بسته نهاده می‌شوند. خصیصه‌ها (attribute) در تگ باز تعریف می‌شوند.

 ساختار یه سند پایه:

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

</body>

</html>
```

**توضیح.**

<div dir="rtl" align="right" markdown="1">

* `<!DOCTYPE html>`: بیاننده html بودن محتوای سند؛
* `<html>...</html>`: تگ اصلی؛ همه‌ی داده‌ها باید درون این باشند.
* `<head>...</head>`: فراداده‌ها بین این نهاده می‌شوند؛
* `<body>...</body>`: همه‌ی محتوا باید بین این نهاده شود؛

</div>

## چند فراداده‌ی پرکاربرد

* توضیح سایت: `<meta name="description" content="یه توضیح کوتاه از سایتمان که در موتورها نموده می‌شود.">`
* تعیین رفتار خزنده‌ی جستجوگرها: `<meta name="robots" content="index,follow">` یا `<meta name="robots" content="noindex,nofollow">`
* تعیین تارنماک: `<link rel="icon" href="path_to_favicon">`
* تنظیم عرض صفحه متناسب با نمایشگر: `<meta name="viewport" content="width=device-width, initial-scale=1.0">`

## بیانوشتن (comment)

بیانوشته‌ها می‌توانند بین <span dir="ltr" markdown="1">`<!--  -->`</span> در یک یا چند خط نهاده شوند.

```html
<body>
    <!-- here can add comment -->
</body>
```

## عنصرهای پایه

تعریف درست تگ‌ها بر روی رتبه‌ی سئوی سایت اثر مثبت دارد.

**توجه.** تگ‌ها را می‌توان درون هم لاناند.

## عنصرهای معنادار

در HTML5 بعضی از عنصرها دارا معنی خاص است. به این معنی که نمونده‌ی نوع محتوایشان هستند.

**چند عنصر معنادار پرکاربرد.**

<div dir="rtl" align="right" markdown="1">

* `header`: نمودنده‌ی سرایند. هر `article` یا `section` می‌تواند سرآیند خودش را داشته باشد.
  * `nav`: نمودنده‌ی ناوبری سایت.
* `section`: نمودنده‌ی یه گروه عنصر همانند.
  * `h2-h6`: نمودنده‌ی عنوان بخش.
* `figure`: نمودنده‌ی یه محتوای نمایشی.
  * `figcaption`: نمودنده‌ی عنوان تصویر.
  * `img`: نمودنده تصویر.
* `footer`: نمودنده‌ی پای‌آیند. هر `article` یا `section` می‌تواند پای‌آیند خودش را داشته باشد.
* `aside`: نوارکنار.
* `article`: یه بخش توصیفی خودمختار و درارتباط با محتوا.
  * `h2-h6`: عنوان
* `main`: نمودنده‌ی محتوای اصلی یه برگه که در هر صفحه یکی است.

</div>

### تگ‌های بخش‌بندی محتوا

#### سرایند `h1-h6`

برای نوشتن عنوان و زیرعنوان تا ۶ سطح در یک صفحه استفیده می‌شود.

```html
<body>
    <!-- ... -->
    <h1>عنوان اصلی</h1>
    <h2>زیرعنوان اول</h2>
    <h3>زیرعنوان اول زیرعنوان اول</h3>
    <h3>زیرعنوان دوم زیرعنوان اول</h3>
    <h2>زیرعنوان دوم</h2>
    <!-- ... -->
</body>
```

**توجه.** بهتره برای افزایش رتبه سئو عنوان‌ها و زیرعنوان‌ها به‌درستی تعیین شوند.

#### بند `p`

برای نوشتن محتوای هر بند از این تگ می‌استفیم.

```html
<body>
    <!-- ... -->
    <p>متن بند اول. اندازه‌ی این متن توسط مرورگر کوچکتر تعیین می‌شود.</p>
    <!-- ... -->
</body>
```

#### تگ‌های قالب‌بندی محتوا

```html
<body>
    <!-- ... -->
    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان <i>گرافیک است چاپگرها</i> و متون بلکه روزنامه و مجله در ستون.</p>
    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان <strong>گرافیک است چاپگرها</strong> و متون بلکه روزنامه و مجله در ستون.</p>
    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان <small>گرافیک است چاپگرها</small> و متون بلکه روزنامه و مجله در ستون.</p>
    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان <del>گرافیک است چاپگرها</del> و متون بلکه روزنامه و مجله در ستون.</p>
    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان <mark>گرافیک است چاپگرها</mark> و متون بلکه روزنامه و مجله در ستون.</p>
    <!-- ... -->
</body>
```

### لیست

برای نمودن یه لیست از اطلاعات.

```html
<body>
    <!-- ... -->
    <ul>
        <li>Home</li>
        <li>Content</li>
    </ul>

    <ol>
        <li>Item 1</li>
        <li>Item 2</li>
    </ol>
    <!-- ... -->
</body>
```

### جدول

```html
<body>
    <!-- ... -->
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
        </tr>
        <tr>
            <th>User Name 1</th>
            <th>User Last Name 1</th>
            <th>26</th>
        </tr>
    </table>
    <!-- ... -->
</body>
```

### چندرسانه‌ای

برای درج محتوای چندرسانه‌ای مانند تصویر و فیلم از این تگ‌ها می‌استفیم.

#### درج تصویر

```html
<body>
    <!-- ... -->
    <img src="path/to/image" />
    <!-- ... -->
</body>
```

#### درج فیلم

```html
<body>
    <!-- ... -->
    <video autoplay controls src="path/to/video"></video>
    <!-- OR -->
    <video controls>
        <source src="/path/to/video" type="video/mp4" />
        <source src="/path/to/video" type="video/mpg" />
    </video>
    <!-- ... -->
</body>
```

### پیوند (link)

برای پیونداندن یه محتوا به یه تارنما.

```html
<body>
    <!-- ... -->
    <a href="the_link_url">link text</a>
    <a target="_blank" href="the_link_url">link content</a>
    <!-- ... -->
</body>
```

### فرم

برای گرفتن اطلاعات از کاربر.

```html
<body>
    <!-- ... -->
    <form action="where_the_data_most_post">
        <label for="user_name">User Name</label>
        <input maxlength="20" 
            minlength="5" 
            name="user_name" 
            type="text" 
            id="user_name" 
            placeholder="At least 5 characters" 
            required />
        <label for="email">Email</label>
        <input autocomplete="off" name="email" type="email" id="email" />
        <select name="product" id="product">
            <option value="mac_book">Macbook 2020</option>
            <option selected value="mobile_phone">Mobile Phone</option>
        </select>
        <input type="radio" name="items" value="item_1" />
        <input type="radio" name="items" value="item_2" />
        <input type="radio" name="items" value="item_3" />
        <input type="radio" name="items" value="item_4" />
        <textarea name="message" id="message" cols="50" rows="20"></textarea>
        <button type="submit">Submit</button>
    </form>
    <!-- ... -->
</body>
```

## ویژگی

هر عنصر (تگ) دارای ویژگی‌هایی با مقدار پیشفرض است که می‌توان به‌شکل زیر آن‌ها را دگرید:

```html
<!-- ... -->
<h1 class="class_1" id="h1_id"></h1>
<!-- ... -->
```

### چند ویژگی پرکاربرد

<div dir="rtl" align="right" markdown="1">

* `id`: شناسه‌ی یه عنصر که در کل صفحه یکتا باشد.
* `class`: کلاس‌های یه عنصر که معمولا برای استایل‌دهی توسط css استفیده می‌شود.

</div>
