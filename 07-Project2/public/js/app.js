// Selectors
const navbar = document.getElementById("navbar");
const burgerIcon = document.getElementById("burger-icon");
const navWrapper = document.getElementById("nav-wrapper");


// Listener
window.addEventListener('resize', displayHandler, true);
document.addEventListener('DOMContentLoaded', displayHandler);

burgerIcon.addEventListener('click', function (event) {
    navbar.classList.add("open");
});

navWrapper.addEventListener('click', function (event) {
    navbar.classList.remove("open");
});

function displayHandler(event) {
    const newWidth = window.innerWidth;

    if (newWidth < 800) {
        navbar.classList.add('burger');
    } else {
        navbar.classList.remove('burger');
    }
}